package az.ibar.userstories.controller;

import az.ibar.userstories.dto.LoginRequest;
import az.ibar.userstories.dto.LoginResponse;
import az.ibar.userstories.dto.TaskDto;
import az.ibar.userstories.service.TaskService;
import az.ibar.userstories.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService service;
    private final AuthenticationManager authenticationManager;
    private final TaskService taskService;

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request){
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(),request.getPassword())
        );
        return ResponseEntity.ok(service.login(request));
    }
    @GetMapping
    public ResponseEntity<List<TaskDto>> getAll(@RequestHeader("Authorization") String token){

        return ResponseEntity.ok(taskService.getAllTasks(token));
    }

}

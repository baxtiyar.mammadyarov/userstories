package az.ibar.userstories.controller;

import az.ibar.userstories.dto.*;

import az.ibar.userstories.service.TaskService;
import az.ibar.userstories.service.UserService;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminController {
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final TaskService taskService;



    @PostMapping("/add")
    public ResponseEntity<String> addAdmin( @RequestBody UserDto dto){
        return ResponseEntity.ok(userService.addAdmin(dto));
    }
    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request){
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(),request.getPassword())
        );
        return ResponseEntity.ok(userService.login(request));
    }



    @PostMapping("/createUser")
   public ResponseEntity<ResponseCreateUser> createUser(
           @Valid @RequestBody RequestCreateUser user,@RequestHeader("authorization") String token){
        return ResponseEntity.ok(userService.addUser(user,token));
   }

    @PostMapping("/createTask")
   public ResponseEntity<TaskDto> createTask(@RequestBody TaskDto dto,@RequestHeader("authorization") String token){
        return ResponseEntity.ok(taskService.createTask(dto,token));
   }
   @GetMapping
   public ResponseEntity<List<TaskDto>> getAll(@RequestHeader("authorization") String token){
        return ResponseEntity.ok(taskService.getAllTasks(token));
   }

   @DeleteMapping
    public void deleteUser(@PathVariable Long id,@RequestHeader("authorization") String token){
        userService.delete(id,token);
   }





}

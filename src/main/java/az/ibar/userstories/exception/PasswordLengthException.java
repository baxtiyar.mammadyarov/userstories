package az.ibar.userstories.exception;

public class PasswordLengthException extends RuntimeException{
    public PasswordLengthException(String message) {
        super(message);
    }
}

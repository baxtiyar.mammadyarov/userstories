package az.ibar.userstories.exception;

public class TaskTitleAlreadyExists extends RuntimeException{
    public TaskTitleAlreadyExists(String message) {
        super(message);
    }
}

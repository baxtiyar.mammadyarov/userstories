package az.ibar.userstories.exception;

public class OrganizationNameExistsException extends RuntimeException{
    public OrganizationNameExistsException(String message) {
        super(message);
    }
}

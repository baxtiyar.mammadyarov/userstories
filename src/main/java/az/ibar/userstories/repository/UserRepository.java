package az.ibar.userstories.repository;

import az.ibar.userstories.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    boolean existsByUsername(String username);

    boolean existsByOrganizationName(String username);

    boolean existsByEmail(String email);

    UserEntity findByUsername(String username);

//    @Query(value = " select * from users where id=?1 and organization=?2 ", nativeQuery = true)
    Optional<UserEntity> findByIdAndOrganizationName(Long id, String organizationName);


    UserEntity getByUsername(String username);

    List<UserEntity> findAllByTaskId(long id);
}

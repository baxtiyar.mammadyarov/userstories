package az.ibar.userstories.repository;


import az.ibar.userstories.entity.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<TaskEntity, Long> {


    List<TaskEntity> findAllByOrganizationName(String organizationName);

    boolean existsByTitle(String title);

    Optional<TaskEntity> findByIdAndOrganizationName(Long id, String organizationName);
}

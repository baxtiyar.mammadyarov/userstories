package az.ibar.userstories.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name = "tasks")
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "title", nullable = false, unique = true)
    private String title;
    @Column(name = "description", nullable = false, unique = true)
    private String description;
    @Column(name = "deadline", nullable = false)
    private LocalDate deadline;
    @Column(name = "status", nullable = false)
    private boolean status;
    @Column(name = "organizationName", nullable = false)
    private String organizationName;

}

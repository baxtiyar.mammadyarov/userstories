package az.ibar.userstories.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "organization", nullable = false)
    private String organizationName;
    @Column(name = "phone", nullable = false)
    private String phoneNumber;
    @Column(name = "address", nullable = false)
    private String address;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "surname", nullable = false)
    private String surname;
    @Column(name = "username", nullable = false, unique = true)
    private String username;
    @Column(name = "email", nullable = false, unique = true)
    private String email;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "status", nullable = false)
    private Status status;
    @Column(name = "enable", nullable = false)
    private boolean isEnable;
    @Column(name = "createDate", nullable = false)
    private LocalDate createDate;
    @ManyToOne(targetEntity = TaskEntity.class)
    @Column(name = "task_id")
    private Long taskId;

}
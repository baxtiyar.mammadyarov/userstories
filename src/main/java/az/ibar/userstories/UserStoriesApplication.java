package az.ibar.userstories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserStoriesApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserStoriesApplication.class, args);
    }

}

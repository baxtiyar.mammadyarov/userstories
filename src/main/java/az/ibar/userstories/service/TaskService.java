package az.ibar.userstories.service;


import az.ibar.userstories.dto.TaskDto;
import az.ibar.userstories.entity.TaskEntity;
import az.ibar.userstories.entity.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface TaskService {


    List<TaskDto> getAllTasks(String token);

    TaskDto createTask(TaskDto dto, String token);
    void delete(Long id ,String token);
}

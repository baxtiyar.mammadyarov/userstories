package az.ibar.userstories.service;

import az.ibar.userstories.dto.Mail;
import org.springframework.stereotype.Service;

@Service
public interface MailService {

    void sendMail(Mail mail);
}

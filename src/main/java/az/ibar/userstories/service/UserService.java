package az.ibar.userstories.service;

import az.ibar.userstories.dto.*;


import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;


@Service
public interface UserService extends UserDetailsService {

    String addAdmin(UserDto dto);

    LoginResponse login(LoginRequest request);

    ResponseCreateUser addUser(RequestCreateUser user, String token);

    void delete(Long id, String token);
}

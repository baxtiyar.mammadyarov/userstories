package az.ibar.userstories.service.impl;

import az.ibar.userstories.dto.Mail;
import az.ibar.userstories.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private final JavaMailSender mailSender;

    @Override
    public void sendMail(final Mail mail) {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setSubject(mail.getSubject());
            message.setText(mail.getContent());
            message.setTo(mail.getTo());
            message.setFrom(mail.getFrom());
            mailSender.send(message);
    }
}


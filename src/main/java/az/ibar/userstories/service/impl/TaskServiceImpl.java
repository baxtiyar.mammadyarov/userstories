package az.ibar.userstories.service.impl;

import az.ibar.userstories.dto.Mail;
import az.ibar.userstories.dto.TaskDto;
import az.ibar.userstories.dto.TaskUser;
import az.ibar.userstories.entity.Status;
import az.ibar.userstories.entity.TaskEntity;
import az.ibar.userstories.entity.UserEntity;
import az.ibar.userstories.exception.*;
import az.ibar.userstories.logger.MyLogger;
import az.ibar.userstories.repository.TaskRepository;
import az.ibar.userstories.repository.UserRepository;
import az.ibar.userstories.security.JwtUtil;
import az.ibar.userstories.service.MailService;
import az.ibar.userstories.service.TaskService;
import lombok.RequiredArgsConstructor;


import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository repository;
    private final UserRepository userRepo;
    private final JwtUtil jwtUtil;
    private final MyLogger logger = MyLogger.getLogger(TaskServiceImpl.class);
    private final MailService mailService;


    @Override
    public List<TaskDto> getAllTasks(String token) {
        logger.info("all tasks method started ");
        UserEntity user = getUserNameFromToken(token);
        logger.info("all tasks method ended ");
        return repository
                .findAllByOrganizationName(user.getOrganizationName())
                .stream().map(this::convertEntityToDto).collect(Collectors.toList());
    }

    @Override
    public TaskDto createTask(TaskDto dto, String token) {

        UserEntity admin = getUserNameFromToken(token);

        if (!admin.getStatus().equals(Status.ADMIN) || admin == null) {
            logger.error(" Your status is not admin");
            throw new StatusException(" Your status is not admin");
        }
        if (repository.existsByTitle(dto.getTitle())) {
            logger.error("Task Title Already Exists");
            throw new TaskTitleAlreadyExists("Task Title Already Exists");
        }
        TaskEntity task = convertDtoToEntity(dto, admin.getOrganizationName());
        task = repository.save(task);
        List<UserEntity> userEntityList = getEntityList(dto.getUsers());

        sendAllMail(userEntityList, task, admin);

        return createTaskResponse(task, userEntityList);
    }

    @Override
    public void delete(Long id, String token) {
        logger.info("task delete method  started");
        UserEntity user = getUserNameFromToken(token);

        if (!user.getStatus().equals(Status.ADMIN) || user == null) {
            logger.error("Your status is not admin");
            throw new StatusException(" Your status is not admin ");
        }
        TaskEntity entity = repository
                .findByIdAndOrganizationName(id, user.getOrganizationName())
                .orElseThrow(() -> {
                    logger.error("task not found");
                    throw new TaskNotFoundException("task not found");
                });
        entity.setStatus(false);
        repository.save(entity);
        userRepo.findAllByTaskId(entity.getId()).stream().forEach(userEntity -> userEntity.setTaskId(null));
        logger.info("user delete method ended");
    }



    private String sendAllMail(List<UserEntity> entities, TaskEntity taskEntity, UserEntity admin) {
        for (UserEntity user : entities) {
            Mail mail = Mail.builder()
                    .from(admin.getEmail())
                    .to(user.getEmail())
                    .content(taskEntity.getTitle())
                    .subject(taskEntity.getDescription())
                    .build();
            mailService.sendMail(mail);
        }
        logger.info("Email sent success");
        return "success";
    }

    private TaskEntity convertDtoToEntity(TaskDto dto, String organizationName) {
        return TaskEntity.builder()
                .organizationName(organizationName)
                .deadline(dto.getDeadline())
                .description(dto.getDescription())
                .status(true)
                .title(dto.getTitle())
                .build();
    }

    private TaskDto createTaskResponse(TaskEntity entity, List<UserEntity> userEntities) {
        List<TaskUser> users = new ArrayList<>();
        for (UserEntity user : userEntities) {
            user.setTaskId(entity.getId());
            users.add(new TaskUser(user.getUsername()));
            userRepo.save(user);
        }
        TaskDto taskDto = convertEntityToDto(entity);
        taskDto.setUsers(users);
        return taskDto;
    }
    private List<UserEntity> getEntityList(List<TaskUser> list) {
        List<UserEntity> entityList = new ArrayList<>();
        for (TaskUser dto : list) {
            entityList.add(userRepo.getByUsername(dto.getUsername()));
        }
        return entityList;
    }

    private TaskDto convertEntityToDto(TaskEntity entity) {
        return TaskDto.builder()
                .deadline(entity.getDeadline())
                .description(entity.getDescription())
                .title(entity.getTitle())
                .status(entity.isStatus())
                .build();
    }


    private UserEntity getUserNameFromToken(String token) {
        logger.info(" getUserNameFromToken method started");
        String username = jwtUtil.getUsernameFromToken(token.substring(7));
        logger.info(" getUserNameFromToken method started");
        return userRepo.findByUsername(username);
    }

}

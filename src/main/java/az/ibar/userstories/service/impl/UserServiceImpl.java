package az.ibar.userstories.service.impl;

import az.ibar.userstories.dto.*;
import az.ibar.userstories.entity.Status;
import az.ibar.userstories.entity.UserEntity;
import az.ibar.userstories.exception.*;
import az.ibar.userstories.logger.MyLogger;
import az.ibar.userstories.repository.UserRepository;
import az.ibar.userstories.security.JwtUtil;
import az.ibar.userstories.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;

@Service
@RequiredArgsConstructor

public class UserServiceImpl implements UserService {

    private final UserRepository repo;
    private final JwtUtil jwtUtil;
    private final MyLogger logger = MyLogger.getLogger(UserServiceImpl.class);


    private final PasswordEncoder encoder;

    public PasswordEncoder getEncoder() {
        return encoder;
    }

    @Override
    public String addAdmin(UserDto dto) {
        logger.info("user addAdmin method  started");
        if (repo.existsByOrganizationName(dto.getOrganizationName())) {
            logger.error("Organization name already exists");
            throw new OrganizationNameExistsException("Organization name already exists ");

        }
        if(dto.getPassword().length()<5){
            throw new PasswordLengthException(" password is less than six characters");
        }
        logger.info("user addAdmin method  ended");
        return createUser(dto, Status.ADMIN);
    }

    @Override
    public void delete(Long id, String token) {
        logger.info("user delete method  started");
        UserEntity user = getUserNameFromToken(token);

        if (!user.getStatus().equals(Status.ADMIN) || user == null) {
            logger.error("Your status is not admin");
            throw new StatusException(" Your status is not admin ");
        }
        user = repo
                .findByIdAndOrganizationName(id, user.getOrganizationName())
                .orElseThrow(() -> {
                    logger.error("user not found");
                    throw new UserNotFoundException("user not found");
                });
        user.setEnable(false);
        repo.save(user);
        logger.info("user delete method ended");
    }

    @Override
    public LoginResponse login(LoginRequest request) {
        logger.info("user login started");
        var user = loadUserByUsername(request.getUsername());
        String token = jwtUtil.generateToken(user);
        logger.info("user login ended");
        return new LoginResponse(token);


    }

    public ResponseCreateUser addUser(RequestCreateUser user, String token) {
        logger.info("adduser  started");
        UserEntity userEntity = getUserNameFromToken(token);
        if (userEntity.getStatus().equals(Status.ADMIN) || user != null) {
            UserDto newUser = UserDto.builder()
                    .name(user.getName())
                    .surname(user.getSurname())
                    .username(user.getUsername())
                    .password(userEntity.getOrganizationName())
                    .email(user.getEmail())
                    .phoneNumber(userEntity.getPhoneNumber())
                    .organizationName(userEntity.getOrganizationName())
                    .address(userEntity.getAddress())
                    .build();
            String newUsername = createUser(newUser, Status.USER);
            userEntity = repo.findByUsername(newUsername);
            logger.info("adduser  ended");
            return ResponseCreateUser.builder()
                    .password(userEntity.getPassword())
                    .username(userEntity.getUsername())
                    .build();
        } else {
            logger.error("Your status is not admin");
            throw new StatusException(" Your status is not admin ");
        }

    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = repo.findByUsername(username);
        if (userEntity == null) {
            logger.error("Username not found");
            throw new UsernameNotFoundException("Username not found");
        }

        return new User(userEntity.getUsername(), userEntity.getPassword(), new ArrayList<>());
    }

    private String createUser(UserDto dto, Status status) {
        if (repo.existsByUsername(dto.getUsername())) {
            logger.error("Username already exists");
            throw new UsernameExistsException("Username already exists");
        }
        if (repo.existsByEmail(dto.getEmail())) {
            logger.error("Email already exists");
            throw new EmailExistsException("Email already exists");
        }
        UserEntity entity = UserEntity.builder()
                .organizationName(dto.getOrganizationName())
                .username(dto.getUsername())
                .name(dto.getName())
                .surname(dto.getSurname())
                .email(dto.getEmail())
                .password(encoder.encode(dto.getPassword()))
                .address(dto.getAddress())
                .phoneNumber(dto.getPhoneNumber())
                .createDate(LocalDate.now())
                .status(status)
                .isEnable(true)
                .build();

        entity = repo.save(entity);

        return entity.getUsername();
    }

    private UserEntity getUserNameFromToken(String token) {

        String username = jwtUtil.getUsernameFromToken(token.substring(7));
        return repo.findByUsername(username);
    }

}
